Name: wdirname
Version: 1.0
Release: 0
License: MIT
Summary: Name of the current directory
BuildArch: noarch
BuildRoot: %{_specdir}/%{name}-%{version}-build
Requires: /bin/bash

%description
Name of the current directory

%install
mkdir -p %{buildroot}%{_bindir}
install -m 755 %{_specdir}/../src/wdirname %{buildroot}%{_bindir}/

%files
%defattr(-,root,root)
%{_bindir}/wdirname

%changelog
